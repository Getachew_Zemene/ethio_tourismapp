import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tab_bar/Hero/HeroPage.dart';

// ignore: camel_case_types
class Detail_Info extends StatefulWidget {
  final heroTag, title, description;

  const Detail_Info({Key key, this.heroTag, this.title, this.description})
      : super(key: key);

  @override
  _Detail_InfoState createState() => _Detail_InfoState();
}

// ignore: camel_case_types
class _Detail_InfoState extends State<Detail_Info> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          leading: IconButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            icon: Icon(Icons.arrow_back_ios),
            color: Colors.white,
          ),
          elevation: 0.0,
          title: Text(widget.title,
              style: TextStyle(
                  fontFamily: "Montserrat",
                  fontSize: 24.0,
                  color: Colors.white,
                  fontWeight: FontWeight.bold)),
          centerTitle: true,
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.more_horiz),
              onPressed: () {},
              color: Colors.white,
            )
          ],
        ),
        body: ListView.builder(
          shrinkWrap: true,
          itemCount: 1,
          scrollDirection: Axis.vertical,
          itemBuilder: (context, position) => Container(
              child: Wrap(children: <Widget>[
            Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
              Padding(
                padding: EdgeInsets.all(10.0),
                child: Stack(children: <Widget>[
                  Container(
                      height: MediaQuery.of(context).size.height - 400,
                      width: MediaQuery.of(context).size.width,
                      child: Image.network(widget.heroTag, fit: BoxFit.fill)),
                  Padding(
                      padding:
                          EdgeInsets.only(left: 280.0, top: 300.0, right: 20.0),
                      child: IconButton(
                        icon: Icon(Icons.fullscreen),
                        iconSize: 60.0,
                        color: Colors.blue,
                        onPressed: () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => HeroPage(
                                  herotag: widget.heroTag,
                                  title: widget.title)));
                        },
                      )),
                  Padding(
                      padding:
                          EdgeInsets.only(left: 10.0, top: 300.0, right: 280.0),
                      child: IconButton(
                        icon: Icon(Icons.location_on),
                        iconSize: 50.0,
                        color: Colors.pink,
                        onPressed: () {},
                      )),
                ]),
              ),
            ]),
            Padding(
                padding: EdgeInsets.all(10.0),
                child: ExpansionTile(
                  backgroundColor: Colors.blueGrey,
                  leading: Icon(
                    Icons.details,
                    color: Colors.pink,
                  ),
                  trailing: Icon(Icons.arrow_drop_down),
                  title: Text("Below are the Description of " + widget.title),
                  children: <Widget>[
                    Text(widget.description,
                        style: TextStyle(
                            fontFamily: "Montserrat",
                            fontSize: 18.0,
                            color: Colors.white,
                            fontWeight: FontWeight.normal)),
                  ],
                )),
          ])),
        ));
  }
}
