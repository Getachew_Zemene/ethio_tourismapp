import 'package:flutter/cupertino.dart';
import "package:flutter/material.dart";

class HeroPage extends StatefulWidget {
  final String herotag, title;

  HeroPage({this.herotag, this.title});

  @override
  _HeroPageState createState() => _HeroPageState();
}

class _HeroPageState extends State<HeroPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        title: Text(widget.title + " fulImage",
            style: TextStyle(
                fontFamily: "Montserrat",
                fontSize: 24.0,
                color: Colors.white,
                fontWeight: FontWeight.bold)),
        centerTitle: true,
        leading: IconButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          icon: Icon(Icons.arrow_back_ios),
          color: Colors.white,
        ),
      ),
      body: Container(
        padding: EdgeInsets.all(5.0),
        child: Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            child: Hero(
                tag: widget.herotag,
                child: Image.network(
                  widget.herotag,
                  fit: BoxFit.fill,
                ))),
      ),
    );
  }
}
