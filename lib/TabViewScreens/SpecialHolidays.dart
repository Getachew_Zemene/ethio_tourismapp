import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tab_bar/Model/Posts.dart';
import 'package:tab_bar/UploadPages/UploadHoliday.dart';
import 'package:tab_bar/detail_information/Detail_Info.dart';

class SpecialHolidays extends StatefulWidget {
  @override
  _SpecialHolidaysState createState() => _SpecialHolidaysState();
}

class _SpecialHolidaysState extends State<SpecialHolidays> {
  List<Posts> postsList = [];

  @override
  void initState() {
    super.initState();

    DatabaseReference postsRef =
        FirebaseDatabase.instance.reference().child("HolidayData");

    postsRef.once().then((DataSnapshot snapshot) {
      // ignore: non_constant_identifier_names
      var KEYS = snapshot.value.keys;
      // ignore: non_constant_identifier_names
      var DATA = snapshot.value;

      postsList.clear();

      for (var indivisualKey in KEYS) {
        Posts posts = Posts(
          DATA[indivisualKey]['date'],
          DATA[indivisualKey]['image'],
          DATA[indivisualKey]['description'],
          DATA[indivisualKey]['fuldescription'],
          DATA[indivisualKey]['name'],
          DATA[indivisualKey]['time'],
        );
        postsList.add(posts);
      }

      setState(() {
        print("Total Posts : ${postsList.length}");
      });
    });
  }

  Widget postsUI(String date, String image, String name, String description,
      String fuldesc) {
    return Card(
      elevation: 12.0,
      margin: EdgeInsets.only(left: 10.0, top: 10.0, bottom: 10.0),
      shape: RoundedRectangleBorder(
        side: BorderSide(color: Theme.of(context).primaryColor),
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: Container(
        padding: EdgeInsets.all(12.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  date,
                  style: Theme.of(context).textTheme.subtitle,
                  textAlign: TextAlign.center,
                ),
                Text(
                  name,
                  style: Theme.of(context).textTheme.subtitle,
                  textAlign: TextAlign.center,
                ),
              ],
            ),
            Container(
                height: 250.0,
                width: MediaQuery.of(context).size.width,
                child: GestureDetector(
                  child: Hero(
                      tag: image,
                      child: Container(
                          child: Image.network(image, fit: BoxFit.fill))),
                  onTap: () {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => Detail_Info(
                              heroTag: image,
                              title: name + " Detail",
                              description: "About $name \n" + fuldesc,
                            )));
                  },
                )),
            SizedBox(
              height: 12.0,
            ),
            Text(
              description,
              style: TextStyle(
                fontStyle: FontStyle.italic,
                fontWeight: FontWeight.w500,
              ),
              textAlign: TextAlign.center,
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Stack(children: <Widget>[
      new Container(
        child: postsList.length == 0
            ? Center(
                child: CircularProgressIndicator(),
              )
            : ListView.builder(
                itemBuilder: (_, index) {
                  return postsUI(
                      postsList[index].date,
                      postsList[index].image,
                      postsList[index].name,
                      postsList[index].description,
                      postsList[index].fuldescription);
                },
                itemCount: postsList.length,
              ),
      ),
      new Container(
        padding:
            EdgeInsets.only(left: 260.0, top: 480.0, right: 20.0, bottom: 10.0),
        child: FloatingActionButton.extended(
          //isExtended: true,
          tooltip: "Add Holiday data to firebase",
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) {
                return UploadHoliday();
              }),
            );
          },
          icon: Icon(Icons.add),
          label: Text(""),
          shape: CircleBorder(),
          backgroundColor: Colors.green,
        ),
      )
    ]);
  }
}
