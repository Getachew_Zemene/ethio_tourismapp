import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tab_bar/Authentication.dart';
import 'package:tab_bar/Mapping.dart';

void main() => runApp(MyApp());

// ignore: must_be_immutable
class MyApp extends StatelessWidget {
  TabController tabController;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        title: "Ethio Tourism",
        home: MappingPage(
          auth: Auth(),
        ));
  }
}
