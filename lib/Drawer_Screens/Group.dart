import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tab_bar/UploadPages/HomePage.dart';

class Group extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: GroupInfo(),
    );
  }
}

class GroupInfo extends StatefulWidget {
  @override
  _GroupInfoState createState() => _GroupInfoState();
}

class _GroupInfoState extends State<GroupInfo> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        title: new Text("Group Members"),
        leading: IconButton(
          onPressed: () {
            Navigator.of(context).push(MaterialPageRoute(builder: (context) {
              return HomePage();
            }));
          },
          icon: Icon(Icons.arrow_back_ios),
          color: Colors.white,
        ),
      ),
      body: ListView(
        padding: EdgeInsets.only(left: 10.0),
        children: <Widget>[
          Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Center(
                child: RichText(
                    softWrap: true,
                    textDirection: TextDirection.ltr,
                    textAlign: TextAlign.left,
                    text: TextSpan(
                      style: DefaultTextStyle.of(context).style,
                      children: <TextSpan>[
                        TextSpan(
                            text: "Developers Information",
                            style: TextStyle(
                                fontSize: 25.0,
                                color: Colors.lightBlue,
                                fontFamily: "montserrat",
                                fontWeight: FontWeight.bold)),
                      ],
                    )),
              ),
              Text(
                "Group Members          ID\n",
                style: TextStyle(
                  fontSize: 25.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
              Text(
                " Getachew Zemene   1010737\n",
                style: TextStyle(
                  fontSize: 20.0,
                ),
              ),
              Text(
                "Fasil Getie        1010712\n",
                style: TextStyle(
                  fontSize: 20.0,
                ),
              ),
              Text(
                "Abdisa Idris       1010753\n",
                style: TextStyle(
                  fontSize: 20.0,
                ),
              ),
              Text(
                "Engida Birhanu     1010589\n",
                style: TextStyle(
                  fontSize: 20.0,
                ),
              ),
              Text(
                "Hanna Samual       1011067\n",
                style: TextStyle(
                  fontSize: 20.0,
                ),
              ),
              Text(
                "Natinal Tadesse    0902955\n",
                style: TextStyle(
                  fontSize: 20.0,
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}
