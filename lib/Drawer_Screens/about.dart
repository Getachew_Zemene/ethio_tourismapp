import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tab_bar/UploadPages/HomePage.dart';

class About extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: AboutAll(),
    );
  }
}

class AboutAll extends StatefulWidget {
  @override
  _AboutAllState createState() => _AboutAllState();
}

class _AboutAllState extends State<AboutAll> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        title: new Text("About"),
        leading: IconButton(
          onPressed: () {
            Navigator.of(context).push(MaterialPageRoute(builder: (context) {
              return HomePage();
            }));
          },
          icon: Icon(Icons.arrow_back_ios),
          color: Colors.white,
        ),
      ),
      body: ListView(
        padding: EdgeInsets.only(left: 10.0),
        children: <Widget>[
          Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Center(
                child: RichText(
                    softWrap: true,
                    textDirection: TextDirection.ltr,
                    textAlign: TextAlign.left,
                    text: TextSpan(
                      style: DefaultTextStyle.of(context).style,
                      children: <TextSpan>[
                        TextSpan(
                            text: "Android Group Project",
                            style: TextStyle(
                                fontSize: 25.0,
                                color: Colors.lightBlue,
                                fontFamily: "montserrat",
                                fontWeight: FontWeight.bold)),
                        TextSpan(
                            text: "    Title:",
                            style: TextStyle(
                                fontSize: 25.0,
                                color: Colors.black87,
                                fontFamily: "montserrat",
                                fontWeight: FontWeight.bold)),
                        TextSpan(
                            text: "Ethio Tourism",
                            style: TextStyle(
                                fontSize: 32.0,
                                color: Colors.purpleAccent,
                                fontFamily: "montserrat",
                                fontWeight: FontWeight.bold)),
                      ],
                    )),
              ),
              RichText(
                  softWrap: true,
                  overflow: TextOverflow.fade,
                  textDirection: TextDirection.ltr,
                  textAlign: TextAlign.left,
                  text: TextSpan(
                      style: DefaultTextStyle.of(context).style,
                      children: <TextSpan>[
                        TextSpan(
                            text: "This application needs an Internet conection"
                                " so in order to see the result please check you are connected to internet"
                                " using either wifi or mobile data.and at first time you must create an acount "
                                " using your email and pass word then the result is shown.",
                            style: TextStyle(
                                fontSize: 20.0,
                                color: Colors.lightBlue,
                                fontFamily: "montserrat",
                                fontWeight: FontWeight.bold))
                      ])),
              RichText(
                  softWrap: true,
                  overflow: TextOverflow.fade,
                  textDirection: TextDirection.ltr,
                  textAlign: TextAlign.left,
                  text: TextSpan(
                      style: DefaultTextStyle.of(context).style,
                      children: <TextSpan>[
                        TextSpan(
                            text: "Primary Developers",
                            style: TextStyle(
                                fontSize: 25.0,
                                color: Colors.pink,
                                fontFamily: "montserrat",
                                fontWeight: FontWeight.bold))
                      ])),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding:
                        EdgeInsets.only(left: 10.0, top: 23.0, right: 10.0),
                    child: Container(
                      padding: EdgeInsets.all(3.0),
                      color: Colors.amber,
                      height: 220,
                      width: 150,
                      child: Image(
                        image: AssetImage(
                          "assets/getch.jpg",
                        ),
                        fit: BoxFit.fill,
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 5.0, top: 23.0, right: 5.0),
                    child: Container(
                      padding: EdgeInsets.all(3.0),
                      color: Colors.amber,
                      height: 220,
                      width: 150,
                      child: Image(
                        image: AssetImage(
                          "assets/faya.jpg",
                        ),
                        fit: BoxFit.fill,
                      ),
                    ),
                  ),
                ],
              ),
              Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.all(5.0),
                      child: Text(
                        "Getachew Zemene",
                        style: TextStyle(fontSize: 20.0, color: Colors.blue),
                      ),
                    ),
                    Padding(
                        padding: EdgeInsets.only(right: 50.0),
                        child: Text(
                          "Fasil Getie",
                          style: TextStyle(fontSize: 20.0, color: Colors.blue),
                        ))
                  ]),
            ],
          )
        ],
      ),
    );
  }
}
