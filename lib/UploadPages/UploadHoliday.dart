import 'dart:io';

import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:tab_bar/TabViewScreens/SpecialHolidays.dart';

class UploadHoliday extends StatefulWidget {
  @override
  _UploadHolidayState createState() => _UploadHolidayState();
}

class _UploadHolidayState extends State<UploadHoliday> {
  File holidayImage;
  String name, description;
  String url;
  final formKey = new GlobalKey<FormState>();
  Widget button;

  @override
  initState() {
    super.initState();
  }

  Future getImage() async {
    var holidayImage = await ImagePicker.pickImage(source: ImageSource.gallery);

    setState(() {
      this.holidayImage = holidayImage;
    });
  }

  bool validateAndSave() {
    final form = formKey.currentState;

    if (form.validate()) {
      form.save();
      return true;
    } else {
      return false;
    }
  }

  void uploadStatusImage() async {
    if (validateAndSave()) {
      final StorageReference postImageRef =
          FirebaseStorage.instance.ref().child("HolidayImage");
      var timeKey = DateTime.now();

      final StorageUploadTask uploadTask =
          postImageRef.child(timeKey.toString() + ".jpg").putFile(holidayImage);

      var imageUrl = await (await uploadTask.onComplete).ref.getDownloadURL();
      url = imageUrl.toString();
      print("Image URL : $url");
      gotoHomePage();
      saveToDatabase(url);
    }
  }

  void saveToDatabase(url) {
    var dbTimeKey = DateTime.now();

    var formatDate = DateFormat('MMM  d, yyyy');
    var formatTime = DateFormat('EEEE, hh:mm aaa');

    String date = formatDate.format(dbTimeKey);
    String time = formatTime.format(dbTimeKey);

    DatabaseReference reference = FirebaseDatabase.instance.reference();

    var data = {
      "image": url,
      "name": name,
      "description": description,
      "date": date,
      "time": time
    };
    reference.child("HolidayData").push().set(data);
  }

  void gotoHomePage() {
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      return SpecialHolidays();
    }));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Upload Holiday Data",
          style: TextStyle(color: Colors.white),
        ),
        centerTitle: true,
      ),
      body: Center(
        child: holidayImage == null ? Text("Select an Image") : enableUpload(),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: getImage,
        tooltip: 'Add Image',
        child: Center(child: Icon(Icons.add_a_photo)),
        backgroundColor: Colors.blue[300],
      ),
    );
  }

  Widget enableUpload() {
    return SingleChildScrollView(
      child: Container(
        margin: EdgeInsets.all(16.0),
        child: Form(
          key: formKey,
          child: Column(
            children: <Widget>[
              Image.file(
                holidayImage,
                height: 320.0,
                width: 640.0,
              ),
              SizedBox(
                height: 16.0,
              ),
              TextFormField(
                decoration: InputDecoration(labelText: "Name:"),
                validator: (value) {
                  return value.isEmpty ? "name is required" : null;
                },
                onSaved: (value) {
                  return name = value;
                },
              ),
              SizedBox(
                height: 16.0,
              ),
              TextFormField(
                decoration: InputDecoration(labelText: "Description"),
                validator: (value) {
                  return value.isEmpty ? "Description is required" : null;
                },
                onSaved: (value) {
                  return description = value;
                },
              ),
              SizedBox(
                height: 16.0,
              ),
              RaisedButton(
                elevation: 12.0,
                child: Text("Add Post"),
                textColor: Colors.white,
                color: Colors.blue[300],
                onPressed: () {
                  uploadStatusImage();
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}
