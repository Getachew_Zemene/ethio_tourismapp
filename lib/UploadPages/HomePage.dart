import 'dart:io';

import 'package:firebase_admob/firebase_admob.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:share/share.dart';
import 'package:tab_bar/Authentication.dart';
import 'package:tab_bar/Drawer_Screens/Group.dart';
import 'package:tab_bar/Drawer_Screens/about.dart';
import 'package:tab_bar/Drawer_Screens/settings.dart';
import 'package:tab_bar/TabViewScreens/Culture.dart';
import 'package:tab_bar/TabViewScreens/FamousePersons.dart';
import 'package:tab_bar/TabViewScreens/Fauna.dart';
import 'package:tab_bar/TabViewScreens/Flora.dart';
import 'package:tab_bar/TabViewScreens/GeniousBookRecorded.dart';
import 'package:tab_bar/TabViewScreens/Hotels.dart';
import 'package:tab_bar/TabViewScreens/Places.dart';
import 'package:tab_bar/TabViewScreens/SpecialHolidays.dart';

class HomePage extends StatefulWidget {
  final AuthImplementation auth;
  final VoidCallback onSignedOut;

  HomePage({this.auth, this.onSignedOut});

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  TabController tabController;

  @override
  Widget build(BuildContext context) {
    FirebaseAdMob.instance
        .initialize(appId: "ca-app-pub-4741101921771008~9322263899")
        .then((response) {
      myInterstitial
        ..load()
        ..show(
          anchorOffset: 0.0,
          anchorType: AnchorType.bottom,
        );
    });
    return WillPopScope(
        onWillPop: () async => false,
        child: DefaultTabController(
          length: 8,
          child: Scaffold(
            appBar: AppBar(
              title: Text("Ethio Tourism"),
              centerTitle: true,
              bottom: new TabBar(
                controller: tabController,
                isScrollable: true,
                indicatorColor: Colors.red,
                tabs: <Widget>[
                  Tab(
                    icon: Icon(Icons.home),
                    text: "Places",
                  ),
                  Tab(
                    icon: Icon(Icons.ac_unit),
                    text: "Flora",
                  ),
                  Tab(
                    icon: Icon(Icons.assessment),
                    text: "Fauna",
                  ),
                  Tab(
                    icon: Icon(Icons.filter),
                    text: "Culture",
                  ),
                  Tab(
                    icon: Icon(Icons.hotel),
                    text: "Exclusive Hotels",
                  ),
                  Tab(
                    icon: Icon(Icons.folder_special),
                    text: "Special Holidays",
                  ),
                  Tab(
                    icon: Icon(Icons.toys),
                    text: "UNISCO Heritages",
                  ),
                  Tab(icon: Icon(Icons.person), text: "Famouse Persons"),
                ],
              ),
            ),
            drawer: Drawer(
              child: ListView(
                children: <Widget>[
                  new UserAccountsDrawerHeader(
                    accountName: Text(
                      "Ethio Tourism",
                      style: TextStyle(
                          fontSize: 25.0,
                          color: Colors.white,
                          fontWeight: FontWeight.bold),
                    ),
                    accountEmail: Text("contact:getchze1221@gmail.com"),
                    decoration: BoxDecoration(
                        color: Colors.blue,
                        image: DecorationImage(
                          image: AssetImage("assets/ethiopia.jpg"),
                          fit: BoxFit.fill,
                        )),
                  ),
                  InkWell(
                    child: ListTile(
                      title: Text("Home Page"),
                      leading: Icon(
                        Icons.home,
                        color: Colors.pink,
                      ),
                    ),
                    onTap: () {
                      Navigator.pop(context);
                    },
                  ),
                  InkWell(
                    child: ListTile(
                      title: Text("Group"),
                      leading: Icon(
                        Icons.group,
                        color: Colors.green,
                      ),
                    ),
                    onTap: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) {
                        return Group();
                      }));
                    },
                  ),
                  InkWell(
                    child: ListTile(
                      title: Text("Settings"),
                      leading: Icon(
                        Icons.settings,
                        color: Colors.yellow,
                      ),
                    ),
                    onTap: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) {
                        return Setting();
                      }));
                    },
                  ),
                  Divider(),
                  InkWell(
                    child: ListTile(
                      title: Text("Rate Us"),
                      leading: Icon(
                        Icons.rate_review,
                        color: Colors.red,
                      ),
                    ),
                    onTap: () {},
                  ),
                  InkWell(
                    child: ListTile(
                      title: Text("Share"),
                      leading: Icon(
                        Icons.share,
                        color: Colors.red,
                      ),
                    ),
                    onTap: () {
                      Share.share("Flutter Application Share");
                    },
                  ),
                  InkWell(
                    child: ListTile(
                      title: Text("About"),
                      leading: Icon(
                        Icons.help,
                        color: Colors.blue,
                      ),
                    ),
                    onTap: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) {
                        return About();
                      }));
                    },
                  ),
                  InkWell(
                    child: ListTile(
                      title: Text("Log Out"),
                      leading: Icon(
                        Icons.clear,
                        color: Colors.blue,
                      ),
                    ),
                    onTap: () {
                      logout();
                    },
                  ),
                  InkWell(
                    child: ListTile(
                      title: Text("Exit"),
                      leading: Icon(
                        Icons.close,
                        color: Colors.red,
                      ),
                    ),
                    onTap: () {
                      exit(0);
                    },
                  ),
                  Divider(),
                  InkWell(
                    child: ListTile(
                      title: Text("Dark Theme"),
                      trailing: Switch(
                        value: false,
                        onChanged: (changedTheme) {},
                      ),
                      leading: Icon(
                        Icons.brightness_2,
                        color: Colors.black87,
                      ),
                    ),
                    onTap: () {},
                  ),
                ],
              ),
            ),
            body: TabBarView(
              controller: tabController,
              dragStartBehavior: DragStartBehavior.start,
              children: <Widget>[
                Places(),
                Flora(),
                Fauna(),
                Culture(),
                Hotels(),
                SpecialHolidays(),
                TopGeniousRecorded(),
                FamousPersons(),
              ],
            ),
          ),
        ));
  }

  void logout() async {
    try {
      await widget.auth.signOut();
      widget.onSignedOut();
    } catch (e) {
      print(e.toString());
    }
  }
}

MobileAdTargetingInfo targetingInfo = MobileAdTargetingInfo(
  keywords: <String>['flutterio', 'beautiful apps'],
  contentUrl: 'https://flutter.io',
  // ignore: deprecated_member_use
  birthday: DateTime.now(),
  childDirected: false,
  // ignore: deprecated_member_use
  designedForFamilies: false,
  // ignore: deprecated_member_use
  gender: MobileAdGender.male,
  // or MobileAdGender.female, MobileAdGender.unknown
  testDevices: <String>[], // Android emulators are considered test devices
);

InterstitialAd myInterstitial = InterstitialAd(
  adUnitId: InterstitialAd.testAdUnitId,
  targetingInfo: targetingInfo,
  listener: (MobileAdEvent event) {
    print("InterstitialAd event is $event");
  },
);
